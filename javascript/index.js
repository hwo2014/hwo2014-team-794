var net = require("net");
var JSONStream = require('JSONStream');
var fs = require('fs');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

var max_corner_speed = 0.9;
var prevCarData;

var shouldLog = false;
var pieces;
var lanes;
var bestCombination;
var switchCountIndex = 0;
var lapCounter = 0;

var trackToUse = "usa";

var fd = fs.openSync("log.txt", 'w');
var history = fs.openSync("hist_"+trackToUse+".dat", 'a');

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  return joinRace(trackToUse);
});

function joinRace(track) {
  return send(
    {"msgType": "joinRace", "data": { "botId": { "name": botName, "key": botKey }, "trackName": track, "carCount": 1 }}
  );
}

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};


function calculateTrackInfo(data) {
  for(var i = 0; i < data.data.race.track.pieces.length; i++) {
    var piece = data.data.race.track.pieces[i];
    var lanes = data.data.race.lanes;
    if(piece.angle) {
      piece.estimatedLength = (Math.PI * piece.radius * 2) * (Math.abs(piece.angle) / 360.0);
    }
  }

  return data;
}

var lastData = null;
var lastTick = 0;
function calculateTotalPositionAndSpeed(data) {
  if(lastData == null) {
    lastData = data;
  }

  for(var i = 0; i < data.data.length; i++) {
    var distance = 0;
    if(data.data[i].piecePosition.pieceIndex === lastData.data[i].piecePosition.pieceIndex) {
      distance = data.data[i].piecePosition.inPieceDistance - lastData.data[i].piecePosition.inPieceDistance;
      data.data[i].velocity = distance / (data.gameTick - lastTick);
      data.data[i].acceleration = (data.data[i].velocity - lastData.data[i].velocity) / (data.gameTick - lastTick);
    }
    else {
      //when switching pieces, we use the same values as last time, because we don't know the exact length of the pieces
      data.data[i].velocity = lastData.data[i].velocity;
      data.data[i].acceleration = lastData.data[i].acceleration;
    }
  }

  lastTick = data.gameTick;
  lastData = data;

  return data;
}

jsonStream = client.pipe(JSONStream.parse());

var lastTickActedUpon = 0;

jsonStream.on('data', function(data) {
  if (data.msgType === 'gameInit') {
    data = calculateTrackInfo(data);
    track = data.data.race.track;
    pieces = track.pieces;
    lanes = track.lanes;
    getShortestLane();

    addDistanceToCornerToTrackPieces(pieces);
    //console.log(pieces);
    laps = data.data.race.raceSession.laps;

  }
  else if (data.msgType === 'carPositions') {
    data = calculateTotalPositionAndSpeed(data);

    if (data.gameTick == lastTickActedUpon && lastTickActedUpon > 0)
      return;

    lastTickActedUpon = data.gameTick;
    for (var i = 0; i < data.data.length; i++) {
      if (data.data[i].id.color === ourColor) {

        var ourCar = data.data[i];
        var curPieceIndex = ourCar.piecePosition.pieceIndex;

        var switchIndex = (pieces[curPieceIndex+1] === undefined) ? 0 : curPieceIndex+1;
        if (shouldSendSwitchMessage(curPieceIndex, switchIndex, switchCountIndex)) {
          var switchDirection = "Right";
          if (bestCombination[switchCountIndex] == 1) {
            switchDirection = "Right";
          } else {
            switchDirection = "Left";
          }
          send({
            msgType: "switchLane",
            data: switchDirection,
            gameTick: data.gameTick
          });
          //console.log("switching " + switchDirection + " at " + switchIndex + " curr ("+curPieceIndex+")");
          prevCarData = ourCar;
          switchCountIndex++;
          if (switchCountIndex >= bestCombination.length)
            switchCountIndex = 0;
          return;
        }

        send({
          msgType: "throttle",
          data: max_corner_speed,
          gameTick: data.gameTick
        });

        if (prevCarData && prevCarData.velocity > 0 && (pieces[curPieceIndex].radius) &&  Math.abs(Math.floor(ourCar.angle)) > 10) {
          var curAngle = Math.abs(Math.floor(ourCar.angle));
          var angleClass = "medium";
          if (curAngle <= 10)
            angleClass = "none";
          else if (curAngle > 10 && curAngle <= 20)
            angleClass = "low";
          else if (curAngle > 20 && curAngle <= 30)
            angleClass = "medium";
          else if (curAngle > 30 && curAngle <= 40)
            angleClass = "medium_high";
          else if (curAngle > 50 && curAngle <= 50)
            angleClass = "high";
          else if (curAngle > 50)
            angleClass = "danger";

          if (shouldLog)
            fs.write(history, "\n{\"input\": {\"v\": " + ourCar.velocity / 10
              //+", \"t\": " + max_corner_speed
              //+ ", \"a\": " + (((ourCar.acceleration + 1) / 2))
              + ", \"r\": " + ((pieces[curPieceIndex].radius)/1000)
              +"}, \"output\": {"
              + "\""+angleClass+"\": 1"  + "}}"
              + ",");
        }


        prevCarData = ourCar;
      }
    }

  } else {
    if (data.msgType === 'join') {
      console.log('Joined')
    } else if (data.msgType === 'gameStart') {
      console.log('Race started');
    } else if (data.msgType === 'yourCar') {
      console.log('myCar ' + data.data.color);
      ourColor = data.data.color;
    } else if (data.msgType === 'gameEnd') {
      console.log('Race ended');
    } else if (data.msgType === 'lapFinished') {
      lapCounter++;
      console.log("laptime " + data.data.lapTime.millis);
    }
    send({
      msgType: "ping",
      data: {}
    });
  }
  if (shouldLog)
    fs.write(fd, JSON.stringify(data, null, 8));
});


var addDistanceToCornerToTrackPieces = function(pieces) {
  var dist = 0;
  var firstCornerIndex = 0;

  for (var i = 0; i < pieces.length; i++) {
    if (pieces[i].length) {

    } else {
      firstCornerIndex = i;
      break;
    }
  }
  var foundCornerFromReverse = false;
  for (var i = pieces.length-1; i >= 0; i--) {
    if (pieces[i].radius) {
      foundCornerFromReverse = true;
      dist = 0;
    } else if (!foundCornerFromReverse) {
      pieces[i].distanceToCorner = dist+firstCornerIndex;
      dist++;
    } else {
      pieces[i].distanceToCorner = dist;
      dist++;
    }
  }
}

function shouldSendSwitchMessage( curPieceIndex, switchIndex, switchCountIndex ){
  return pieces[switchIndex].switch
            && ((prevCarData && prevCarData.piecePosition.pieceIndex != curPieceIndex)
              || (lapCounter == 0 && curPieceIndex == 0 && switchCountIndex == 0))
}

function getShortestLane() {
  var switchCount = 0;
  var results = new Array();
  var finalShortestLength = 9999999999999;
  for (var p = 0; p < pieces.length; p++) {
    if (pieces[p].switch) {
      switchCount++;
    }
  }

  var trackLaneCombinations = binaryCombos(switchCount);
  for (var comb = 0; comb < trackLaneCombinations.length; comb++) {
    for (var i = 0; i < lanes.length; i++) {
      var totalLength = 0;
      var currLane = lanes[i].index;
      var switchedCounter = 0;
      for (var p = 0; p < pieces.length; p++) {
        var piece = pieces[p];
        if (piece.switch) {
          currLane = trackLaneCombinations[comb][switchedCounter];
          switchedCounter++;
        }
        var distOffset = lanes[currLane].distanceFromCenter;
        if (piece.length) {
          totalLength += piece.length;
        }
        else {
          var actualRadius = piece.radius;
          if (piece.angle < 0) { // left
            actualRadius += distOffset;
          } else {
            actualRadius -= distOffset;
          }
          actualRadius = Math.abs(actualRadius);
          totalLength += (Math.PI * actualRadius * 2) * (Math.abs(piece.angle) / 360.0);
        }
      }
      //console.log("lane " + i + " " + totalLength + " combination " + trackLaneCombinations[comb]);
      if (totalLength < finalShortestLength) {
        finalShortestLength = totalLength;
        bestCombination = trackLaneCombinations[comb];
      }
    }
  }
  console.log("===== shortest track with " + bestCombination + " length " + finalShortestLength);
}

// http://zacg.github.io/blog/2013/08/02/binary-combinations-in-javascript/
function binaryCombos(n){
    var result = [];
    for(y=0; y<Math.pow(2,n); y++){
        var combo = [];
        for(x=0; x<n; x++){
            //shift bit and and it with 1
            if((y >> x) & 1)
                combo.push(1);
             else
                combo.push(0);
        }
        result.push(combo);
    }
    return result;
}

